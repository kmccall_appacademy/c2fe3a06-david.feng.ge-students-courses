class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_reader :first_name, :last_name, :courses

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    unless new_course.students.include?(self)
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    course_hash = Hash.new(0)
    @courses.each do |course|
      course_hash[course.department] += course.credits
    end
    course_hash
  end


end
